# test-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
cross-env VUE_APP_API_KEY=Basic token npm start
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
